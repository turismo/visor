   
(function () {
    var module = angular.module("angular-google-maps", ["google-maps"]);
}());

function geoServer ($scope, $http, $filter) {
  $http.get('json/datos.json').success(function(data) {
     $scope.places = data.features;
     $scope.markersProperty = data.features;
     $scope.filteredMarkersProperty = $scope.markersProperty;

    this.total = data.features.length;
    $scope.total = this.total;

    var provs = [];
        for (var i = 0; i < this.total; i++){
            provs[i] = data.features[i].properties.provincia;
        }

    var sorted_provs = provs.sort();
    
    $scope.provincias = [];
        for (var i = 0; i < provs.length; i++){
            if (sorted_provs[i+1] != sorted_provs[i]){
                $scope.provincias.push(sorted_provs[i]);
            }
        }
  }); //End Data Grab

  $scope.$watch( 'orderProp', function ( val ) {

        $scope.filteredMarkersProperty = $filter('filter')($scope.markersProperty, val);
        $scope.zoomProperty = 11;
        calcFocus();

    });

    $scope.showAll = function($event){
        $scope.orderProp ="0";
        //$scope.filteredMarkersProperty = $scope.places;
        

        $scope.items = [];
        for (var i = 0; i < $scope.places.length; i++){
            
            //var item = { id: id, name: name, type: type };
            //data.items.push(item);

            var item = $scope.places[i].geometry.coordinates;
            var data = {latitude: item[1],longitude: item[0]};
            //$scope.variable = data;
            $scope.items.push(data);
        }
        $scope.filteredMarkersProperty =  $scope.items;
        //$scope.variable = "showAll" + $scope.places.length;

        $scope.zoomProperty = 11;
        calcFocus();
    }

     $scope.select = function($event){

        //$scope.variable = "COOD" + $event.geometry.coordinates[0];

        var theName = $event.properties.nombre;
        var lat = $event.geometry.coordinates[1];
        var lng = $event.geometry.coordinates[0];
        var json = {latitude: lat,longitude: lng};
        $scope.filteredMarkersProperty = [json];
        $scope.centerProperty.lat = lat;
        $scope.centerProperty.lng = lng;
        $scope.zoomProperty = 10;
        calcFocus();

        //$scope.variable = $scope.filteredMarkersProperty;

     }
    function calcFocus(){
        var lats = []
        var longs = []
        var counter = [];

        for(i=0; i<$scope.filteredMarkersProperty.length; i++)
        {
            //$scope.variable = "LON " + $scope.filteredMarkersProperty[i].geometry.coordinates[0];
            lats[i] = $scope.filteredMarkersProperty[i].geometry.coordinates[1];
            longs[i] = $scope.filteredMarkersProperty[i].geometry.coordinates[0];
        }

        var latCount = 0;
        var longCount = 0;

        for (i=0; i<lats.length; i++){
            latCount += lats[i];
            longCount += longs[i];
        }

        latCount = latCount / lats.length;
        longCount = longCount / longs.length;
        $scope.centerProperty.lat = latCount;
        $scope.centerProperty.lng = longCount;
    };


    angular.extend($scope, {

        /** the initial center of the map */
        centerProperty: {
            lat:-0.166364,
            lng:-78.469365
        },

        /** the initial zoom level of the map */
        zoomProperty: 7,

        /** list of markers to put in the map */

        markersProperty : [],

        // These 2 properties will be set when clicking on the map - click event
        clickedLatitudeProperty: null,  
        clickedLongitudeProperty: null
    });

}